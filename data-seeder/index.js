require('dotenv').config({
    path: `./env-files/${process.env.NODE_ENV || 'development'}.env`,
});
const mongoose = require("mongoose");
const Movies = require("./../models/movies");
const mockMovies = require('./../mock-data/movies');
const MovieComments = require("../models/movie_comments");
const mockMovieComments = require('./../mock-data/movie-comments');

//insert movies and comments
const startProcess = () => {
    Movies.insertMany(mockMovies).then(r => {
        console.log('inserted movies successfully');
        MovieComments.insertMany(mockMovieComments).then(rc => {
            console.log('inserted movie comments');
            mongoose.disconnect();
        }).catch(err => {
            console.error('error while inserting movie comments', err);
        });
    }).catch(err => {
        console.error('error while inserting movies', err);
    });
};

// clean collections
const dropCollections = () => new Promise((resolve, reject) => {
    Movies.collection.drop();
    MovieComments.collection.drop();
    resolve('collections dropped');
}); 

//connect mongoose
mongoose
    .connect(process.env.MONGODB_URI, { useNewUrlParser: true })
    .catch(err => {
        console.error('error while connecting mongodb', err.stack);
        process.exit(1);
    })
    .then(() => {
        console.log("connected to db in development environment");
        dropCollections().then(() => {
            console.log('collection dropped');
            console.log('starting inserting mock-data');
            startProcess();
        });
    });

