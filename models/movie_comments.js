const mongoose = require('mongoose');
const { Schema } = mongoose;
const ObjectId = Schema.ObjectId;

const movie_comments = new Schema({
  movie_id: ObjectId,
  name: String,
  comment: String,
  user_id: ObjectId,
  status: {
    type: String,
    default: 'active',
    enum: {
      values: ['active', 'inactive'],
      message: '{VALUE} is not supported'
    }
  },
}, {
  timestamps: true
});

module.exports = mongoose.model('movie_comments', movie_comments);
