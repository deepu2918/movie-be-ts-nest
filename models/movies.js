const mongoose = require('mongoose');
const { Schema } = mongoose;
const ObjectId = Schema.ObjectId;

const movies = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  release_date: {
    type: Date,
    required: true
  },
  rating: {
    type: Number,
    required: true,
    max: 5,
    min: 1,
  },
  ticket_price: {
    type: Number,
    required: true
  },
  country: {
    type: String,
    required: true
  },
  genre: {
    type: [String],
    required: true
  },
  photo: {
    type: String,
    required: true
  },
  status: {
    type: String,
    default: 'active',
    enum: {
      values: ['active', 'inactive'],
      message: '{VALUE} is not supported'
    }
  },
  user_id: ObjectId
}, {
  timestamps: true
});

module.exports = mongoose.model('movies', movies);