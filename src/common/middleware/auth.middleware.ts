import { Injectable, NestMiddleware, HttpStatus } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { JwtService } from '@nestjs/jwt';
import * as _ from 'lodash';


@Injectable()
export class AuthMiddleware implements NestMiddleware {

  constructor(private readonly jwtService: JwtService) { }

  async use(req: Request, res: Response, next: NextFunction) {
    const token = req.headers.authorization;
    this.jwtService.verifyAsync(token).then((tokenVerify) => {
      req.body.name = tokenVerify.name;
      req.body.user_id = tokenVerify._id;
      next();
    }).catch((tokenError) => {
      console.log('token error', tokenError);
      res.status(HttpStatus.UNAUTHORIZED).json({
        message: "User token expired",
      });
    });
  }
}
