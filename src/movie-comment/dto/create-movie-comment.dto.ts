import { IsNotEmpty } from 'class-validator';

export class CreateMovieCommentDTO {
    readonly name: string;
    @IsNotEmpty()
    movie_id: string;
    @IsNotEmpty()
    readonly comment: string;
    readonly user_id: string;
    readonly create_at: Date;
}