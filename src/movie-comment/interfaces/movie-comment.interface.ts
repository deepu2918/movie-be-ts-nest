import { Document, Types } from 'mongoose';

export interface MovieComment extends Document {
    readonly name: string;
    readonly movie_id: Types.ObjectId;
    readonly comment: Date;
    readonly user_id: Types.ObjectId;
    readonly status: string;
    readonly create_at: Date;
    readonly updated_at: Date;
}