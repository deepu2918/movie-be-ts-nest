import { Test, TestingModule } from '@nestjs/testing';
import { MovieCommentController } from './movie-comment.controller';
import { MovieCommentService } from './movie-comment.service';

describe('MovieCommentController', () => {
  let controller: MovieCommentController;
  let mockService = {
    getAllMovieComment: jest.fn(dto => [{
      _id: 1,
      name: 'anudeep',
      movie_id: '1',
      comment: 'good move',
      user_id: '1',
      create_at: new Date()
    }]),
    getMovieCommentCount: jest.fn(() => 1),
    addMovieComment: jest.fn(dto => {
      return {
        _id: Date.now(),
        ...dto,
      }
    }),
  };

  const mockResponse = () => {
    const res = {
      status: jest.fn(res => {
        return {
          json: (res) => res,
        }
      }),
    }
    return res;
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MovieCommentController],
      providers: [MovieCommentService],
    }).overrideProvider(MovieCommentService).useValue(mockService).compile();

    controller = module.get<MovieCommentController>(MovieCommentController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should return movie comments', () => {
    const res = mockResponse();
    controller.getAllMovieComments(res, 1, {}).then(data => {
      expect(data).toEqual({
        msg: 'success',
        statusCode: 200,
        data: {
          movieComments: [{
            _id: 1,
            name: 'anudeep',
            movie_id: '1',
            comment: 'good move',
            user_id: '1',
            create_at: expect.any(Date)
          }],
          count: 1,
          loadMore: true,
        }
      });
    });
  });

  it('should add a movie comment', () => {
    const dto = {
      name: 'anudeep',
      movie_id: '1',
      comment: 'good move',
      user_id: '1',
      create_at: new Date()
    };
    const res = mockResponse();
    controller.addMovieComment(res, "1", dto).then(data => {
      expect(data).toEqual({
        statusCode: 200,
        msg: 'Movie has been created successfully',
        data: {
          movieComment: {
            _id: expect.any(Number),
            name: 'anudeep',
            movie_id: "1",
            comment: 'good move',
            user_id: '1',
            create_at: expect.any(Date)
          }
        }
      });
    });
  });

  it('should return response structure', () => {
    expect(controller.structureRes(200, 'success', {})).toEqual({
      statusCode: 200,
      msg: 'success',
      data: {}
    });
  });

});