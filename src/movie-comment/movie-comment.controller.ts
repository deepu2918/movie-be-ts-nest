import { Controller, Get, Res, Req, HttpStatus, Post, Body, Put, Query, NotFoundException, Delete, Param } from '@nestjs/common';
import { MovieCommentService } from './movie-comment.service';
import { CreateMovieCommentDTO } from './dto/create-movie-comment.dto';
import { MovieComment } from './interfaces/movie-comment.interface';

@Controller('films')
export class MovieCommentController {
    constructor(private movieCommentService: MovieCommentService) { }

    structureRes = (statusCode = 200, msg = 'successfully fetched records', data = {}) => {
        return {
            statusCode, msg, data
        };
    };

    // add a movie comment
    @Post(':movieId/comments/create')
    async addMovieComment(@Res() res, @Param('movieId') movieId, @Body() createMovieCommentDTO: CreateMovieCommentDTO): Promise<MovieComment> {
        createMovieCommentDTO.movie_id = movieId;
        const movieComment = await this.movieCommentService.addMovieComment(createMovieCommentDTO);
        return res.status(HttpStatus.OK).json(this.structureRes(200, 'Movie has been created successfully', { movieComment }));
    }

    
    
    // Retrieve movies list
    @Get(':movieId/comments')
    async getAllMovieComments(@Res() res, @Param('movieId') movieId, @Query() query) {
        const pageSize = 1;
        let page = query.page ? query.page : 1;
        page -= 1;
        const movieComments = await this.movieCommentService.getAllMovieComment(movieId, page, pageSize);
        const movieCommentsCount = await this.movieCommentService.getMovieCommentCount(movieId);
        return res.status(HttpStatus.OK).json(this.structureRes(200, 'success', { movieComments, count: movieCommentsCount, loadMore: pageSize === movieComments.length }));
    }
}
