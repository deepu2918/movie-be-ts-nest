import { Module } from '@nestjs/common';
import { MovieCommentService } from './movie-comment.service';
import { MovieCommentController } from './movie-comment.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { MovieCommentSchema } from './schemas/movie-comment.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'MovieComment', schema: MovieCommentSchema }])
  ],
  providers: [MovieCommentService],
  controllers: [MovieCommentController]
})
export class MovieCommentModule {}
