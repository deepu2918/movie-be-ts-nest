import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { MovieCommentService } from './movie-comment.service';

describe('MovieCommentService', () => {
  let service: MovieCommentService;
  const mockMovieCommentMongoose = {
    create: jest.fn().mockImplementation(dto => {
      dto._id = Date.now();
      return Promise.resolve(dto)
    }),
    countDocuments: jest.fn(() => {
      return {
        exec: () => {
          return 1;
        }
      }
    }),
    find: jest.fn().mockImplementation(dto => {
      return {
        skip: () => {
          return {
            limit: () => {
              return {
                exec: () => {
                  return Promise.resolve([{
                    name: 'anudeep',
                    movie_id: '1',
                    comment: 'good move',
                    user_id: '1',
                    create_at: new Date()
                  }]);
                }
              }
            }
          }
        }
      }
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MovieCommentService, {
        provide: getModelToken('MovieComment'),
        useValue: mockMovieCommentMongoose,
      }],
    }).compile();

    service = module.get<MovieCommentService>(MovieCommentService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a movie comment', () => {
    const dto = {
      name: 'anudeep',
      movie_id: '1',
      comment: 'good move',
      user_id: '1',
      create_at: new Date()
    };
    return service.addMovieComment(dto).then(data => {
      expect(data).toEqual({
        _id: expect.any(Number),
        ...dto
      });
    });
  });

  it('should return count of movies', () => {
    return service.getMovieCommentCount('61214c308cd10ea329cbe84e').then(data => {
      expect(data).toEqual(1);
    });
  });

  it('should return a movies list', () => {
    return service.getAllMovieComment(1, 1, 1).then(data => {
      expect(data).toEqual([{
        name: 'anudeep',
        movie_id: '1',
        comment: 'good move',
        user_id: '1',
        create_at: expect.any(Date)
      }]);
    });
  });

});
