import { Injectable } from '@nestjs/common';
import { Model, Types } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { MovieComment } from './interfaces/movie-comment.interface';
import { CreateMovieCommentDTO } from './dto/create-movie-comment.dto';

@Injectable()
export class MovieCommentService {

    constructor(@InjectModel('MovieComment') private readonly movieCommentModel: Model<MovieComment>) { }

    // fetch all movie comments
    async getAllMovieComment(movieId, page, pageSize): Promise<MovieComment[]> {
        const movieComments = await this.movieCommentModel.find({ movie_id: Types.ObjectId(movieId), status: 'active' }).skip(page).limit(pageSize).exec();
        return movieComments;
    }
    // fetch movie comments count
    async getMovieCommentCount(movieId): Promise<number> {
        const movieCommentCount = await this.movieCommentModel.countDocuments({ movie_id: Types.ObjectId(movieId), status: 'active' }).exec();
        return movieCommentCount;
    }

    // post a single movie comment
    async addMovieComment(createMovieCommentDTO: CreateMovieCommentDTO): Promise<MovieComment> {
        return this.movieCommentModel.create(createMovieCommentDTO);
    }

}
