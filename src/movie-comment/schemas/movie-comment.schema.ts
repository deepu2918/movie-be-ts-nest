import * as mongoose from 'mongoose';
import { Types } from 'mongoose';

export const MovieCommentSchema = new mongoose.Schema({
    movie_id: Types.ObjectId,
    name: String,
    comment: String,
    user_id: Types.ObjectId,
    status: {
        type: String,
        default: 'active',
        enum: {
            values: ['active', 'inactive'],
            message: '{VALUE} is not supported'
        }
    },
}, {
    timestamps: true,
    collection: 'movie_comments'
});

MovieCommentSchema.index({ movie_id: 1, status: 1, created_at: -1 }); // schema level