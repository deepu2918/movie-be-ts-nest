import { IsNotEmpty } from 'class-validator';

export class CreateMovieDTO {
    @IsNotEmpty()
    readonly name: string;
    @IsNotEmpty()
    readonly description: string;
    @IsNotEmpty()
    readonly release_date: Date;
    @IsNotEmpty()
    readonly ticket_price: number;
    @IsNotEmpty()
    rating: number;
    @IsNotEmpty()
    readonly country: string;
    @IsNotEmpty()
    readonly genre: string[];
    @IsNotEmpty()
    photo: string;
}