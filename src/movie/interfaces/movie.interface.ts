import { Document } from 'mongoose';

export interface Movie extends Document {
    readonly name: string;
    readonly description: string;
    readonly release_date: Date;
    readonly ticket_price: number;
    readonly rating: number;
    readonly country: string;
    readonly genre: [string];
    readonly photo: string;
    readonly status: string;
    readonly create_at: Date;
    readonly updated_at: Date;
}