import { Test, TestingModule } from '@nestjs/testing';
import { MovieController } from './movie.controller';
import { MovieService } from './movie.service';

describe('MovieController', () => {
  let controller: MovieController;

  const mockService = {
    getMovie: jest.fn(dto => {
      return {
        name: 'anudeep',
        description: 'anudeep the superman',
        release_date: new Date(),
        ticket_price: 5000,
        rating: 5,
        country: 'India',
        genre: ['Action', 'Family'],
        photo: './anudeep.jpg',
      }
    }),
    getAllMovie: jest.fn(dto => {
      return [{
        name: 'anudeep',
        description: 'anudeep the superman',
        release_date: new Date(),
        ticket_price: 5000,
        rating: 5,
        country: 'India',
        genre: ['Action', 'Family'],
        photo: './anudeep.jpg',
      }]
    }),
    getCountMovie: jest.fn(() => 3),
    addMovie: jest.fn(dto => {
      return {
        message: "Movie has been created successfully",
      }
    }),
  };

  const mockResponse = () => {
    const res = {
      status: jest.fn(res => {
        return {
          json: (res) => res,
        }
      }),
    }
    return res;
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MovieController],
      providers: [MovieService]
    }).overrideProvider(MovieService).useValue(mockService).compile();

    controller = module.get<MovieController>(MovieController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should return a movie', () => {
    const res = mockResponse();
    controller.getMovie(res, "1").then(data => {
      expect(data).toEqual({
        msg: 'success',
        statusCode: 200,
        data: {
          movie: {
            name: 'anudeep',
            description: 'anudeep the superman',
            release_date: expect.any(Date),
            ticket_price: 5000,
            rating: 5,
            country: 'India',
            genre: ['Action', 'Family'],
            photo: './anudeep.jpg',
          }
        }
      });
    });
  });

  it('should return movies list', () => {
    const res = mockResponse();
    controller.getAllMovie(res, 1).then(data => {
      expect(data).toEqual({
        msg: 'success',
        statusCode: 200,
        data: {
          count: 3,
          movies: [
            {
              name: 'anudeep',
              description: 'anudeep the superman',
              release_date: expect.any(Date),
              ticket_price: 5000,
              rating: 5,
              country: 'India',
              genre: ['Action', 'Family'],
              photo: './anudeep.jpg',
            }
          ]
        }
      });
    });
  });

  it('should create a movie', () => {
    const dto = {
      name: 'anudeep',
      description: 'anudeep the superman',
      release_date: new Date(),
      ticket_price: 5000,
      rating: 5,
      country: 'India',
      genre: ['Action', 'Family'],
      photo: './anudeep.jpg',
    };
    const res = mockResponse();
    controller.addMovie(res, dto).then(data => {
      expect(data).toEqual({
        message: "Movie has been created successfully",
        movie: {
          message: "Movie has been created successfully",
        }
      });
    });
  });

  it('should return response structure', () => {
    expect(controller.structureRes(200, 'success', {})).toEqual({
      statusCode: 200,
      msg: 'success',
      data: {}
    });
  });


});
