import { Controller, Get, Res, Req, HttpStatus, Post, Body, Put, Query, NotFoundException, Delete, Param, UploadedFile, UseInterceptors, } from '@nestjs/common';
import { MovieService } from './movie.service';
import { CreateMovieDTO } from './dto/create-movie.dto';
import { Movie } from './interfaces/movie.interface';
import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { editFileName, imageFileFilter } from './../utils/file-upload.utils';

@Controller('films')
export class MovieController {
    constructor(private movieService: MovieService) { }

    // save a movie image
    @UseInterceptors(FileInterceptor('image', {
        storage: diskStorage({
            destination: './static/movie-img',
            filename: editFileName,
        }),
        fileFilter: imageFileFilter,
    }))
    @Post('save-film-image')
    async saveMoveImg(@Req() req, @Res() res, @Body() @UploadedFile() file: Express.Multer.File) {
        return res.status(HttpStatus.OK).json({
            message: "Film image saved successfully",
            fileName: req.body.savedFileName
        });
    }

    // save a movie
    @Post('create')
    async addMovie(@Res() res, @Body() createMovieDTO: CreateMovieDTO): Promise<Movie> {
        const movie = await this.movieService.addMovie(createMovieDTO);
        return res.status(HttpStatus.OK).json({
            message: "Movie has been created successfully",
            movie
        })
    }

    structureRes = (statusCode = 200, msg = 'successfully fetched records', data = {}) => {
        return {
            statusCode, msg, data
        };
    };

    // Retrieve movies list
    @Get()
    async getAllMovie(@Res() res, @Query() query) {
        const movies = await this.movieService.getAllMovie(query.page);
        const moviesCount = await this.movieService.getCountMovie();
        return res.status(HttpStatus.OK).json(this.structureRes(200, 'success', { movies, count: moviesCount }));
    }

    // Fetch a particular movie using ID
    @Get(':movieId')
    async getMovie(@Res() res, @Param('movieId') movieId) {
        const movie = await this.movieService.getMovie(movieId);
        if (!movie) throw new NotFoundException('Movie does not exist!');
        return res.status(HttpStatus.OK).json(this.structureRes(200, 'success', { movie }));
    }
}
