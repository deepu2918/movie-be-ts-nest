import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MovieController } from './movie.controller';
import { MovieService } from './movie.service';
import { MovieSchema } from './schemas/movie.schema';
import { MulterModule } from '@nestjs/platform-express';

@Module({
    imports: [
      MongooseModule.forFeature([{ name: 'Movie', schema: MovieSchema }]),
      MulterModule.register({
        dest: './static/movie-img',
      })
    ],
    controllers: [MovieController],
    providers: [MovieService]
  })
export class MovieModule {}
