import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { MovieService } from './movie.service';

describe('MovieService', () => {
  let service: MovieService;
  const mockMovieMongoose = {
    create: jest.fn().mockImplementation(dto => {
      dto._id = Date.now();
      return Promise.resolve(dto)
    }),
    findById: jest.fn().mockImplementation(dto => Promise.resolve({
      name: 'anudeep',
      description: 'anudeep the superman',
      release_date: new Date("18/05/1990"),
      ticket_price: 5000,
      rating: 5,
      country: 'India',
      genre: ['Action', 'Family'],
      photo: './anudeep.jpg',
    })),
    countDocuments: jest.fn(() => 3),
    find: jest.fn().mockImplementation(dto => {
      return {
        skip: () => {
          return {
            limit: () => {
              return Promise.resolve([{
                name: 'anudeep',
                description: 'anudeep the superman',
                release_date: new Date("18/05/1990"),
                ticket_price: 5000,
                rating: 5,
                country: 'India',
                genre: ['Action', 'Family'],
                photo: './anudeep.jpg',
              }]);
            }
          }
        }
      }
    }),
  };


  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MovieService, {
        provide: getModelToken('Movie'),
        useValue: mockMovieMongoose,
      }],
    }).compile();

    service = module.get<MovieService>(MovieService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a movie', () => {
    const dto = {
      name: 'anudeep',
      description: 'anudeep the superman',
      release_date: new Date(),
      ticket_price: 5000,
      rating: 5,
      country: 'India',
      genre: ['Action', 'Family'],
      photo: './anudeep.jpg',
    };
    return service.addMovie(dto).then(data => {
      expect(data).toEqual({
        _id: expect.any(Number),
        ...dto
      });
    });
  });

  it('should return a movie', () => {
    return service.getMovie("1").then(data => {
      expect(data).toEqual({
        name: 'anudeep',
        description: 'anudeep the superman',
        release_date: expect.any(Date),
        ticket_price: 5000,
        rating: 5,
        country: 'India',
        genre: ['Action', 'Family'],
        photo: './anudeep.jpg',
      });
    });
  });

  it('should return count of movies', () => {
    return service.getCountMovie().then(data => {
      expect(data).toEqual(3);
    });
  });

  it('should return a movies list', () => {
    return service.getAllMovie(1).then(data => {
      expect(data).toEqual([{
        name: 'anudeep',
        description: 'anudeep the superman',
        release_date: expect.any(Date),
        ticket_price: 5000,
        rating: 5,
        country: 'India',
        genre: ['Action', 'Family'],
        photo: './anudeep.jpg',
      }]);
    });
  });
});
