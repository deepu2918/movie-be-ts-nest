import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Movie } from './interfaces/movie.interface';
import { CreateMovieDTO } from './dto/create-movie.dto';

@Injectable()
export class MovieService {

    constructor(@InjectModel('Movie') private movieModel: Model<Movie>) { }

    // fetch all movies
    async getAllMovie(page): Promise<Movie[]> {
        const pageSize = 1;
        page = page ? page : 1;
        page -= 1;
        const movies = await this.movieModel.find({ status: 'active' }).skip(page).limit(pageSize);
        return movies;
    }

    // fetch movies count
    async getCountMovie(): Promise<number> {
        const moviesCount = await this.movieModel.countDocuments({ status: 'active' });
        return moviesCount;
    }

    // Get a single movie
    async getMovie(movieId): Promise<Movie> {
        const movie = await this.movieModel.findById(movieId);
        return movie;
    }

    // post a single movie
    async addMovie(createMovieDTO: CreateMovieDTO): Promise<Movie> {
        return this.movieModel.create(createMovieDTO);
    }
}
