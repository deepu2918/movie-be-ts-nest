import * as mongoose from 'mongoose';

export const MovieSchema = new mongoose.Schema({
    name: {
      type: String,
      required: true
    },
    description: {
      type: String,
      required: true
    },
    release_date: {
      type: Date,
      required: true
    },
    rating: {
      type: Number,
      required: true,
      max: 5,
      min: 1,
    },
    ticket_price: {
      type: Number,
      required: true
    },
    country: {
      type: String,
      required: true
    },
    genre: {
      type: [String],
      required: true
    },
    photo: {
      type: String,
      required: true
    },
    status: {
      type: String,
      default: 'active',
      enum: {
        values: ['active', 'inactive'],
        message: '{VALUE} is not supported'
      }
    },
  }, {
    timestamps: true
  });

  MovieSchema.index({ status: 1, created_at: -1 }); // schema level