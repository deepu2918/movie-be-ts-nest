import { IsEmail, IsNotEmpty } from 'class-validator';

export class CreateUserDTO {
    @IsNotEmpty()
    readonly name: string;
    @IsEmail()
    readonly email: string;
    @IsNotEmpty()
    readonly phone: string;
    @IsNotEmpty()
    password: string;
}