import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema({
    name: {
      type: String,
      required: true
    },
    email: {
      type: String,
      required: true,
      unique: true
    },
    phone: {
      type: String,
      required: true
    },
    password: {
      type: String,
      required: true
    },
    status: {
      type: String,
      default: 'active',
      enum: {
        values: ['active', 'inactive'],
        message: '{VALUE} is not supported'
      }
    },
  }, {
    timestamps: true
});

UserSchema.index({ email: 1, status: 1 }); // schema level