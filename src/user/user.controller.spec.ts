import { JwtService } from '@nestjs/jwt';
import { Test, TestingModule } from '@nestjs/testing';
import { UserController } from './user.controller';
import { UserService } from './user.service';

describe('UserController', () => {
  let controller: UserController;
  let mockService = {
    addUser: jest.fn(dto => {
      return {
        message: "User has been created successfully",
      }
    }),
    login: jest.fn(dto => {
      return {
        message: "User has been verified successfully",
        token: '123',
        name: 'anudeep'
      }
    })
  };

  const mockResponse = () => {
    const res = {
      status: jest.fn(res => {
        return {
          json: (res) => res,
        }
      }),
    }
    return res;
  };

  let mockJwtService = {
    sign: jest.fn((dto, opt) => {
      return 1111111;
    })
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [UserService, JwtService],
    }).overrideProvider(UserService).useValue(mockService).overrideProvider(JwtService).useValue(mockJwtService).compile();

    controller = module.get<UserController>(UserController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create a user', () => {
    const dto = {
      name: "anudeep",
      email: "anudeep@gmail.com",
      phone: "1478523699",
      password: "159753",
    };
    const res = mockResponse();
    controller.addUser(res, dto).then(data => {
      expect(data).toEqual({
        message: "User has been created successfully",
      });
    });
  });

  it('should return login token', () => {
    const dto = {
      name: "anudeep",
      email: "anudeep@gmail.com",
      phone: "1478523699",
      password: "159753",
    };
    const res = mockResponse();
    controller.login(res, dto).then(data => {
      expect(data).toEqual({
        message: "User has been verified successfully",
        token: '123',
        name: 'anudeep'
      });
    });
  });
});
