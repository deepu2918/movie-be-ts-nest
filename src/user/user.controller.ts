import { Controller, Get, Res, HttpStatus, Post, Body, Put, Query, NotFoundException, Delete, Param } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDTO } from './dto/create-user.dto';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';

@Controller('user')
export class UserController {

    saltOrRounds = 10;

    constructor(private userService: UserService,
        private readonly jwtService: JwtService) { }

    // add a user
    @Post('/login')
    async login(@Res() res, @Body() createUserDTO: CreateUserDTO) {
        const user = await this.userService.loginUser(createUserDTO);
        const isMatch = await bcrypt.compare(createUserDTO.password, user.password);
        if (!isMatch) return res.status(HttpStatus.UNAUTHORIZED).json({ message: "Invalid username and pwd" });
        const { _id, name, email } = user;
        const token = this.jwtService.sign({ _id, name, email }, { expiresIn: '1h' });
        return res.status(HttpStatus.OK).json({
            message: "User has been verified successfully",
            token,
            name
        })
    }

    // add a user
    @Post('/register')
    async addUser(@Res() res, @Body() createUserDTO: CreateUserDTO) {
        createUserDTO.password = await bcrypt.hash(createUserDTO.password, this.saltOrRounds);
        const user = await this.userService.addUser(createUserDTO);
        return res.status(HttpStatus.OK).json({
            message: "User has been created successfully",
        })
    }
}
