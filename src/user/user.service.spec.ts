import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from './user.service';

describe('UserService', () => {
  let service: UserService;
  const mockUserMongoose = {
    create: jest.fn().mockImplementation(dto => {
      dto._id = Date.now();
      return Promise.resolve(dto)
    }),
    findOne: jest.fn().mockImplementation(dto => Promise.resolve({ ...dto }))
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserService, {
        provide: getModelToken('User'),
        useValue: mockUserMongoose,
      }],
    }).compile();

    service = module.get<UserService>(UserService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a user', () => {
    const dto = {
      name: "anudeep",
      email: "anudeep@gmail.com",
      phone: "1478523699",
      password: "159753",
    };
    return service.addUser(dto).then(data => {
      expect(data).toEqual({
        _id: expect.any(Number),
        ...dto
      });
    });
  });

  it('should return a user', () => {
    const dto = {
      name: "anudeep",
      email: "anudeep@gmail.com",
      phone: "1478523699",
      password: "159753",
    };
    return service.loginUser(dto).then(data => {
      expect(data).toEqual({
        email: "anudeep@gmail.com",
      });
    });
  });

});
