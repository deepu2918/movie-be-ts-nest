
import { Injectable } from '@nestjs/common';
import { User } from './interfaces/user.interface';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { CreateUserDTO } from './dto/create-user.dto';


@Injectable()
export class UserService {

    constructor(@InjectModel('User') private userModel: Model<User>) { }

    async loginUser(createUserDTO: CreateUserDTO): Promise<User> {
        const user = await this.userModel.findOne({email: createUserDTO.email});
        return user;
    };

    addUser(createUserDTO: CreateUserDTO): Promise<User> {
        return this.userModel.create(createUserDTO);
    };
};
